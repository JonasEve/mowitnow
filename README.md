# mowitnow

## How to initialize

Execute "yarn install" command

## How to use

Set a file in the directory and execute "yarn start" command.

By default file name should be "file.txt" but it can be overrided with "--filename myFile.txt" argument.

List of arguments :

--filename : search the file in the root directory with this name

--logger (console) : enable a type of logger (only "console" exist)

--verbose : if logger is enabled, more informations will be displayed

--optimize : analyse file to remove useless commands

## How to test

Execute "yarn test" command

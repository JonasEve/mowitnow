import {
  Action,
  Command,
  Coordinate,
  Orientation,
  orientations,
  Position,
  shiftCommands,
  TurnCommand,
  turnCommands,
} from "../../domain/types";

export const shiftAction = (gardenSize: Coordinate): Action => ({
  move(currentPosition: Position): Position {
    let newPosition: Position = {
      ...currentPosition,
      coordinate: { ...currentPosition.coordinate },
    };
    switch (currentPosition.orientation) {
      case "N":
        newPosition.coordinate.y++;
        break;
      case "S":
        newPosition.coordinate.y--;
        break;
      case "E":
        newPosition.coordinate.x++;
        break;
      case "W":
        newPosition.coordinate.x--;
        break;
      default:
        throw new Error("Invalid Orientation");
    }
    return canMove(gardenSize, newPosition.coordinate)
      ? newPosition
      : currentPosition;
  },
});

export const turnAction = (command: TurnCommand): Action => ({
  move(currentPosition: Position): Position {
    const index = orientations.indexOf(currentPosition.orientation);
    let newOrientation: Orientation;
    switch (command) {
      case "G":
        if (index === 0)
          newOrientation = orientations[orientations.length - 1] as Orientation;
        else newOrientation = orientations[index - 1] as Orientation;
        break;
      case "D":
        if (index === orientations.length - 1)
          newOrientation = orientations[0] as Orientation;
        else newOrientation = orientations[index + 1] as Orientation;
        break;
      default:
        throw new Error("Invalid Command");
    }
    return {
      ...currentPosition,
      orientation: newOrientation,
    };
  },
});

export function mowerActionFactory(
  gardenSize: Coordinate,
  command: Command
): Action {
  if (turnCommands.includes(command)) return turnAction(command as TurnCommand);
  if (shiftCommands.includes(command)) return shiftAction(gardenSize);
  throw new Error("Invalid Command");
}

export function canMove(maxCoordinate: Coordinate, newCoordinate: Coordinate) {
  return (
    newCoordinate.x >= 0 &&
    newCoordinate.x <= maxCoordinate.x &&
    newCoordinate.y >= 0 &&
    newCoordinate.y <= maxCoordinate.y
  );
}

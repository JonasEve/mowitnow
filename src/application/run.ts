import { ActionFactory, Data, Logger, Position } from "../domain/types";

export const run =
  (actionFactory: ActionFactory, logger?: Logger) =>
  (data: Data): Position => {
    let position = data.mower.initialPos;
    data.mower.commands.forEach((command) => {
      try {
        const action = actionFactory(data.gardenSize, command);
        position = action.move(position);
        logger?.info?.(JSON.stringify(position));
      } catch (err: any) {
        logger?.warning?.(err.message);
      }
    });
    return position;
  };

export type Coordinate = {
  x: number;
  y: number;
};

export type Position = {
  orientation: Orientation;
  coordinate: Coordinate;
};

export type Mower = {
  initialPos: Position;
  commands: Command[];
};

export interface Data {
  gardenSize: Coordinate;
  mower: Mower;
}

// KEEP THIS ORDER
export const orientations = ["N", "E", "S", "W"];
export type Orientation = "N" | "E" | "S" | "W";

export const turnCommands = ["D", "G"];
export type TurnCommand = "D" | "G";
export const shiftCommands = ["A"];
export type ShiftCommand = "A";
export const commands = [...turnCommands, ...shiftCommands];
export type Command = TurnCommand | ShiftCommand;

export interface Action {
  move(currentPosition: Position): Position;
}

export type ActionFactory = (
  gardenSize: Coordinate,
  command: Command
) => Action;

export interface Logger {
  info?: (data: string) => void;
  warning?: (data: string) => void;
  error?: (data: string) => void;
}

import { Logger } from "./domain/types";
import { loggerFactory, LoggerType } from "./infrastructure/logger";
import { start } from "./start";

const args = process.argv.slice(2);
let logger: Logger | undefined;
const loggerType = getArg(args, "logger");
if (loggerType) {
  try {
    logger = loggerFactory(
      loggerType as LoggerType,
      getArg(args, "verbose") ? true : false
    );
  } catch (err: any) {
    console.log(err.message);
  }
}

start({
  logger,
  fileName: getArg(args, "fileName"),
  optimize: getArg(args, "optimize") ? true : false,
});

function getArg(args: string[], name: string): string | undefined {
  const index = args.indexOf(`--${name}`);
  if (index !== -1) return args[index + 1] ?? " ";
}

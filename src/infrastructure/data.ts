import * as fs from "fs";

export type RawMower = {
  initialPos: string;
  commands: string;
};

export interface RawData {
  gardenSize: string;
  mowers: RawMower[];
}

export function initData(mode: "file" | "hardcode", fileName: string): RawData {
  switch (mode) {
    case "file":
      const file = fs.readFileSync(fileName, "utf8");
      return fileToRawData(file);
    case "hardcode":
      return {
        gardenSize: "5 5",
        mowers: [{ initialPos: "1 2 N", commands: "GAGAGAGAA" }],
      };
  }
}

export function fileToRawData(file: string) {
  const lines = file.split("\r\n").filter((line) => line.trim() !== "");
  if (lines.length < 3 || lines.length % 2 === 0)
    throw new Error("Wrong File Format");
  const gardenSize = lines.shift() as string;
  const mowers: RawMower[] = [];
  lines.forEach((line, index) => {
    if (index % 2 === 0) {
      mowers.push({
        initialPos: line,
        commands: lines[index + 1],
      });
    }
  });
  return {
    gardenSize: gardenSize,
    mowers,
  };
}

import { Logger } from "../domain/types";

export type LoggerType = "console";

export function loggerFactory(type: LoggerType, withInfo?: boolean): Logger {
  switch (type) {
    case "console":
      return consoleLogger(withInfo);
    default:
      throw new Error("Invalid Logger Type");
  }
}

export const consoleLogger = (withInfo?: boolean): Logger => ({
  info(data: string) {
    if (withInfo) console.log(`I : ${data}`);
  },
  warning(data: string) {
    console.log(`W : ${data}`);
  },
  error(data: string) {
    console.log(`E : ${data}`);
  },
});

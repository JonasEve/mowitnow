import { RawMower } from "./data";
import {
  Command,
  commands,
  Coordinate,
  Mower,
  Orientation,
  orientations,
  Position,
} from "../domain/types";

export function toCoordinate(coordinate: string): Coordinate {
  const c = coordinate.split(" ");
  if (c.length < 2) throw new Error("Coordinate format error");
  return {
    x: parseInt(c[0]),
    y: parseInt(c[1]),
  };
}

export function toPosition(position: string): Position {
  const c = position.split(" ");
  if (c.length < 3) throw new Error("position format error");
  if (!orientations.includes(c[2])) throw new Error("position format error");
  return {
    coordinate: toCoordinate(position),
    orientation: c[2] as Orientation,
  };
}

export function toCommands(
  commandsString: string,
  withOptimisation?: boolean
): Command[] {
  let stringArray = [
    ...(withOptimisation ? optimizeCommand(commandsString) : commandsString),
  ];
  return stringArray.filter((c) => commands.includes(c)) as Command[];
}

// D then G or G then D is as keep the same orientation
// TODO : improve (DD...GG...) and (GG...DD...) should be removed
export function optimizeCommand(command: string): string {
  return command
    .replace(new RegExp("DG", "g"), "")
    .replace(new RegExp("GD", "g"), "");
}

export function toMower(mower: RawMower, withOptimisation?: boolean): Mower {
  return {
    initialPos: toPosition(mower.initialPos),
    commands: toCommands(mower.commands, withOptimisation),
  };
}

export function positionToString(position: Position) {
  return `${position.coordinate.x} ${position.coordinate.y} ${position.orientation}`;
}

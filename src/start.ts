import { mowerActionFactory } from "./application/actions/mower-actions";
import { run } from "./application/run";
import { Logger } from "./domain/types";
import { initData } from "./infrastructure/data";
import {
  positionToString,
  toCoordinate,
  toMower,
} from "./infrastructure/mappers";

export function start(config: {
  logger?: Logger;
  fileName?: string;
  optimize?: boolean;
}) {
  try {
    const mowerRun = run(mowerActionFactory, config.logger);

    const data = initData("file", config.fileName ?? "file.txt");
    const gardenSize = toCoordinate(data.gardenSize);

    data.mowers.forEach((mower) => {
      try {
        const lastPosition = mowerRun({
          mower: toMower(mower, config.optimize),
          gardenSize,
        });
        console.log(positionToString(lastPosition));
      } catch (err: any) {
        config.logger?.error?.(err.message);
      }
    });
  } catch (err: any) {
    config.logger?.error?.(err.message) ?? console.log(err.message);
  }
}

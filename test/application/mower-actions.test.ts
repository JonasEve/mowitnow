import {
  canMove,
  shiftAction,
  turnAction,
} from "../../src/application/actions/mower-actions";

describe("mower-actions", () => {
  describe("shiftAction", () => {
    test("When move with N orientation, Should be y + 1", async () => {
      //Arrange
      const action = shiftAction({ x: 5, y: 5 });
      //Act
      const result = action.move({
        orientation: "N",
        coordinate: { x: 1, y: 1 },
      });
      //Assert
      expect(result).toEqual({
        orientation: "N",
        coordinate: { x: 1, y: 2 },
      });
    });

    test("When move with N orientation and end of garden, Should not move", async () => {
      //Arrange
      const action = shiftAction({ x: 5, y: 5 });
      //Act
      const result = action.move({
        orientation: "N",
        coordinate: { x: 1, y: 5 },
      });
      //Assert
      expect(result).toEqual({
        orientation: "N",
        coordinate: { x: 1, y: 5 },
      });
    });
  });

  describe("turnAction", () => {
    test("When move with D command and start with N;E;S;W, Should be E;S;W;N orientation", async () => {
      //Arrange
      const action = turnAction("D");
      //Act
      const resultN = action.move({
        orientation: "N",
        coordinate: { x: 1, y: 1 },
      });
      const resultE = action.move({
        orientation: "E",
        coordinate: { x: 1, y: 1 },
      });
      const resultS = action.move({
        orientation: "S",
        coordinate: { x: 1, y: 1 },
      });
      const resultW = action.move({
        orientation: "W",
        coordinate: { x: 1, y: 1 },
      });
      //Assert
      expect(resultN).toEqual({
        orientation: "E",
        coordinate: { x: 1, y: 1 },
      });
      expect(resultE).toEqual({
        orientation: "S",
        coordinate: { x: 1, y: 1 },
      });
      expect(resultS).toEqual({
        orientation: "W",
        coordinate: { x: 1, y: 1 },
      });
      expect(resultW).toEqual({
        orientation: "N",
        coordinate: { x: 1, y: 1 },
      });
    });
    test("When move with G command and start with N;W;S;E, Should be W;S;E;N orientation", async () => {
      //Arrange
      const action = turnAction("G");
      //Act
      const resultN = action.move({
        orientation: "N",
        coordinate: { x: 1, y: 1 },
      });
      const resultW = action.move({
        orientation: "W",
        coordinate: { x: 1, y: 1 },
      });
      const resultS = action.move({
        orientation: "S",
        coordinate: { x: 1, y: 1 },
      });
      const resultE = action.move({
        orientation: "E",
        coordinate: { x: 1, y: 1 },
      });
      //Assert
      expect(resultN).toEqual({
        orientation: "W",
        coordinate: { x: 1, y: 1 },
      });
      expect(resultW).toEqual({
        orientation: "S",
        coordinate: { x: 1, y: 1 },
      });
      expect(resultS).toEqual({
        orientation: "E",
        coordinate: { x: 1, y: 1 },
      });
      expect(resultE).toEqual({
        orientation: "N",
        coordinate: { x: 1, y: 1 },
      });
    });
  });

  describe("canMove", () => {
    test("When newCoordinate > to maxCoordinate, Should return false", async () => {
      //Arrange
      //Act
      const resultY = canMove({ x: 1, y: 1 }, { x: 1, y: 2 });
      const resultX = canMove({ x: 1, y: 1 }, { x: 2, y: 1 });
      //Assert
      expect(resultY).toBe(false);
      expect(resultX).toBe(false);
    });
    test("When newCoordinate < to maxCoordinate, Should return true", async () => {
      //Arrange
      //Act
      const resultY = canMove({ x: 1, y: 2 }, { x: 1, y: 2 });
      const resultX = canMove({ x: 2, y: 1 }, { x: 2, y: 1 });
      //Assert
      expect(resultY).toBe(true);
      expect(resultX).toBe(true);
    });
  });
});

import { start } from "../src/start";

describe("e2e", () => {
  test("When parse file with 3 lines or more, Should return rawData", async () => {
    //Arrange
    console.log = jest.fn();
    const fileName = "file_test.txt";
    //Act
    start({ fileName });
    //Assert
    expect(console.log).toHaveBeenCalledWith("5 1 E");
  });
});

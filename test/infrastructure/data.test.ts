import { fileToRawData, RawData } from "../../src/infrastructure/data";

describe("data", () => {
  describe("fileToRawData", () => {
    test("When parse file with 3 lines or more, Should return rawData", async () => {
      //Arrange
      const file = "5 5\r\n1 2 N\r\nGAGAGAGAA";
      //Act
      const result: RawData = fileToRawData(file);
      //Assert
      expect(result).toEqual({
        gardenSize: "5 5",
        mowers: [{ initialPos: "1 2 N", commands: "GAGAGAGAA" }],
      });
    });
    test("When parse file with less than 3 lines, Should throw exception", async () => {
      //Arrange
      const file = "5 5\r\n1 2 N";
      //Act
      const result = () => fileToRawData(file);
      //Assert
      expect(result).toThrowError();
    });

    test("When parse file with incorrect format, Should throw exception", async () => {
      //Arrange
      const file = "5 5\r\n1 2 N\n\rGAGAGA\n\r2 2 E";
      //Act
      const result = () => fileToRawData(file);
      //Assert
      expect(result).toThrowError();
    });
  });
});

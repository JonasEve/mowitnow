import { optimizeCommand } from "../../src/infrastructure/mappers";

describe("mappers", () => {
  describe("optimizeData", () => {
    test("When parse file with 3 lines or more, Should return rawData", async () => {
      //Arrange
      const commands = "DGGAGAGDGAGAA";
      //Act
      const result = optimizeCommand(commands);
      //Assert
      expect(result).toEqual("GAGAGAGAA");
    });
  });
});
